defmodule Dashroom.Repo do
  use Ecto.Repo,
    otp_app: :dashroom,
    adapter: Ecto.Adapters.Postgres
end
