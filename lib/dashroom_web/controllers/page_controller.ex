defmodule DashroomWeb.PageController do
  use DashroomWeb, :controller

  alias Phoenix.LiveView
  # alias TeacherWeb.AlbumLive.Index

  def index(conn, _params) do
    LiveView.Controller.live_render(conn, DashroomWeb.DashboardLive.Index, session: %{})
  end
end
