defmodule DashroomWeb.DashboardLive.Index do
  use Phoenix.LiveView

  def mount(_session, socket) do
    # albums = Recordings.list_albums()
    clicks = 0
    data = Poison.encode!([[175, 60], [190, 80], [180, 75]])
    {:ok, assign(socket, clicks: clicks, data: data)}
  end

  def render(assigns) do
    DashroomWeb.PageView.render("index.html", assigns)
  end

  # def handle_event(e, values, socket) do
  #   IO.puts inspect(values)
  #   {:noreply, socket}
  # end

  def handle_event("increase_click", %{"amount" => amount} = _values, socket) do
    {amount, ""} = Integer.parse(amount)
    IO.inspect(amount)
    clicks = socket.assigns.clicks + amount
    IO.inspect(clicks)
    {:noreply, assign(socket, clicks: clicks)}
    # album_id = String.to_integer(album_id)
    # {:noreply, assign(socket, editable_id: album_id)}
  end
end
